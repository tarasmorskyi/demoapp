package com.tarasmorskyi.demoapp;

import com.tarasmorskyi.demoapp.domain.interactors.MainInteractor;
import com.tarasmorskyi.demoapp.domain.interactors.MainInteractorImpl;
import com.tarasmorskyi.demoapp.domain.repositories.LocalRepository;
import com.tarasmorskyi.demoapp.domain.repositories.RemoteRepository;
import com.tarasmorskyi.demoapp.domain.repositories.Repositories;
import com.tarasmorskyi.demoapp.model.Page;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import java.util.ArrayList;
import java.util.List;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.MockitoRule;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MainInteractorTest {

  @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();


  @Mock RemoteRepository remote;
  @Mock LocalRepository local;

  @Mock Repositories repositories;


  MainInteractor interactor;

  @Before public void setup(){
    when(repositories.getRemote()).thenReturn(remote);
    when(repositories.getLocal()).thenReturn(local);
    interactor = new MainInteractorImpl(repositories);
  }

  @Test public void getPagesWithAlreadyAvailableToken(){
    when(local.getToken()).thenReturn(Maybe.just("token"));
    List<Page> pageList = new ArrayList<>();
    pageList.add(Page.EMPTY);
    pageList.add(Page.EMPTY);
    pageList.add(Page.EMPTY);
    when(remote.getPages(any())).thenReturn(Maybe.just(pageList));
    interactor.getPosts().test().assertSubscribed().assertNoErrors().assertValue(pageList);
  }

  @Test public void getPagesWithNoToken(){
    when(local.getToken()).thenReturn(Maybe.just(""));
    when(remote.login()).thenReturn(Maybe.just("token"));
    when(local.setToken(any())).thenReturn(Completable.complete());
    List<Page> pageList = new ArrayList<>();
    pageList.add(Page.EMPTY);
    pageList.add(Page.EMPTY);
    pageList.add(Page.EMPTY);
    when(remote.getPages(any())).thenReturn(Maybe.just(pageList));
    interactor.getPosts().test().assertSubscribed().assertNoErrors().assertValue(pageList);
  }
}
