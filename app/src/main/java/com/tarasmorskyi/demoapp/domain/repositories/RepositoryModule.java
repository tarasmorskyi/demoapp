package com.tarasmorskyi.demoapp.domain.repositories;

import com.tarasmorskyi.demoapp.domain.DomainToolsModule;
import dagger.Binds;
import dagger.Module;

@SuppressWarnings({ "StaticMethodOnlyUsedInOneClass", "squid:S1118", "squid:S1610" })
@Module(includes = { DomainToolsModule.class }) public abstract class RepositoryModule {

  @Binds protected abstract LocalRepository localRepository(LocalRepositoryImpl localRepository);

  @Binds
  protected abstract RemoteRepository remoteRepository(RemoteRepositoryImpl remoteRepository);
}
