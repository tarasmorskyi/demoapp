package com.tarasmorskyi.demoapp.domain;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.serjltt.moshi.adapters.Wrapped;
import com.squareup.moshi.Moshi;
import com.tarasmorskyi.demoapp.BuildConfig;
import com.tarasmorskyi.demoapp.domain.storage.StorageModule;
import com.tarasmorskyi.demoapp.utils.MoshiDateAdapter;
import com.tarasmorskyi.demoapp.utils.MyAdapterFactory;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

@SuppressWarnings({ "StaticMethodOnlyUsedInOneClass", "squid:S1118", "squid:S1610" })
@Module(includes = { StorageModule.class}) public abstract class DomainToolsModule {

  private static final int TIMEOUT = 60;

  @Provides @Singleton static Retrofit provideRestAdapter(Moshi moshi, OkHttpClient okHttpClient) {
    Retrofit.Builder builder = new Retrofit.Builder().client(okHttpClient)
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create());
    builder.baseUrl(BuildConfig.SERVER);
    return builder.build();
  }

  @Provides @Singleton static OkHttpClient getOkHttpClient() {
    OkHttpClient.Builder builder = new OkHttpClient.Builder();
    if (BuildConfig.DEBUG) {
      builder.addNetworkInterceptor(new StethoInterceptor());
    }
    return builder.build();
  }
  @Provides @Singleton static Moshi.Builder provideDefaultMoshiBuilder() {
    return new Moshi.Builder()
        .add(new MoshiDateAdapter())
        .add(MyAdapterFactory.create())
        .add(Wrapped.ADAPTER_FACTORY);
  }

  @Provides @Singleton static Moshi provideMoshi(Moshi.Builder moshiBuilder) {
    return moshiBuilder.build();
  }
}
