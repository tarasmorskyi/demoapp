package com.tarasmorskyi.demoapp.domain.repositories;

import io.reactivex.Completable;
import io.reactivex.Maybe;

public interface LocalRepository {

  Maybe<String> getToken();

  Completable setToken(String token);
}
