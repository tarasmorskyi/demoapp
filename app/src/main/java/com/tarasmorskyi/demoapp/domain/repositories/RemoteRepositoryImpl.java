package com.tarasmorskyi.demoapp.domain.repositories;

import com.tarasmorskyi.demoapp.model.LoginResult;
import com.tarasmorskyi.demoapp.model.Page;
import com.tarasmorskyi.demoapp.model.Verse;
import dagger.Reusable;
import io.reactivex.Maybe;
import io.reactivex.schedulers.Schedulers;
import java.util.List;
import javax.inject.Inject;
import retrofit2.Retrofit;
import timber.log.Timber;

@Reusable public class RemoteRepositoryImpl implements RemoteRepository {

  private final RemoteService service;

  @Inject RemoteRepositoryImpl(Retrofit retrofit) {
    this.service = retrofit.create(RemoteService.class);
  }

  @Override public Maybe<String> login() {
    Timber.d("login: []");
    //this data I got from dashboard provided by this service
    return service.login("9UDziDviGKtpRhpFMQloylPG5Mg8kGVa9JAYC5Vf",
        "pdAZM7BOBN2Oa18Hyn5G8WflRhd6rayHdjrXSR4qs9LhHfgZyF", "client_credentials")
        .subscribeOn(Schedulers.io())
        .compose(RxUtils.transformMaybeResult())
        .map(LoginResult::accessToken);
  }

  @Override public Maybe<List<Page>> getPages(String accessToken) {
    return service.getPages("Bearer " + accessToken)
        .subscribeOn(Schedulers.io())
        .compose(RxUtils.transformMaybeResult());
  }

  @Override public Maybe<List<Verse>> getVerses(String accessToken, int chapterNumber) {
    return service.getVerses("Bearer " + accessToken, chapterNumber)
        .subscribeOn(Schedulers.io())
        .compose(RxUtils.transformMaybeResult());
  }
}