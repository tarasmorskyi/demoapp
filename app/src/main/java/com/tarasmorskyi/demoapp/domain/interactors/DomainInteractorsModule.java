package com.tarasmorskyi.demoapp.domain.interactors;

import dagger.Binds;
import dagger.Module;

@SuppressWarnings({ "StaticMethodOnlyUsedInOneClass", "squid:S1118", "squid:S1610" }) @Module
public abstract class DomainInteractorsModule {

  @Binds abstract MainInteractor provideMainInteractor(MainInteractorImpl mainInteractor);

  @Binds abstract DetailsInteractor provideDetailsInteractor(DetailsInteractorImpl detailsInteractor);
}