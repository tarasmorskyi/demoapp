package com.tarasmorskyi.demoapp.domain.storage;

import android.content.Context;
import android.content.SharedPreferences;
import com.squareup.moshi.Moshi;
import javax.annotation.concurrent.ThreadSafe;

@ThreadSafe public class Storage extends MoshiStorage {

  private static Storage instance;

  private Storage(SharedPreferences preferences, Moshi moshi) {
    super(preferences, moshi);
  }

  static synchronized Storage getDefault(Context context, Moshi moshi) {
    if (instance == null) {
      instance = createInstance(context, context.getPackageName(), moshi);
    }
    return instance;
  }

  private static Storage createInstance(Context context, String sharedPreferencesName,
      Moshi moshi) {
    SharedPreferences sharedPreferences = context.getSharedPreferences(
        sharedPreferencesName != null ? sharedPreferencesName
            : context.getPackageName() + "_JsonStorage", Context.MODE_PRIVATE);
    return new Storage(sharedPreferences, moshi);
  }
}
