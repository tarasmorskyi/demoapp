package com.tarasmorskyi.demoapp.domain.repositories;

import com.tarasmorskyi.demoapp.domain.storage.Storage;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import javax.inject.Inject;

import static com.tarasmorskyi.demoapp.utils.Constants.EMPTY_STRING;

public class LocalRepositoryImpl implements LocalRepository {

  private static final String TOKEN = "token";
  private final Storage storage;

  @Inject LocalRepositoryImpl(Storage storage) {
    this.storage = storage;
  }

  @Override public Maybe<String> getToken() {
    return Maybe.fromCallable(() -> storage.getString(TOKEN, EMPTY_STRING));
  }

  @Override public Completable setToken(String token) {
    return Completable.fromAction(() -> storage.setString(TOKEN, token));
  }
}
