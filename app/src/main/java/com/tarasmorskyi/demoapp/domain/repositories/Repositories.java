package com.tarasmorskyi.demoapp.domain.repositories;

import dagger.Lazy;
import dagger.Reusable;
import javax.inject.Inject;

@Reusable public class Repositories {

  private final Lazy<RemoteRepository> remoteRepository;

  private final Lazy<LocalRepository> localRepository;

  @Inject public Repositories(
      Lazy<RemoteRepository> remoteRepository, Lazy<LocalRepository> localRepository) {
    this.remoteRepository = remoteRepository;
    this.localRepository = localRepository;
  }

  public RemoteRepository getRemote() {
    return remoteRepository.get();
  }

  public LocalRepository getLocal() {
    return localRepository.get();
  }
}
