package com.tarasmorskyi.demoapp.domain.interactors;

import com.tarasmorskyi.demoapp.model.Page;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import java.util.List;

public interface MainInteractor {
  Maybe<List<Page>> getPosts();
  Completable logout();
}
