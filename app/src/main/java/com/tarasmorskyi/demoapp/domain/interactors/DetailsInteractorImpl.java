package com.tarasmorskyi.demoapp.domain.interactors;

import com.tarasmorskyi.demoapp.domain.repositories.LocalRepository;
import com.tarasmorskyi.demoapp.domain.repositories.RemoteRepository;
import com.tarasmorskyi.demoapp.domain.repositories.Repositories;
import com.tarasmorskyi.demoapp.model.Verse;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import java.util.List;
import javax.inject.Inject;

import static com.tarasmorskyi.demoapp.utils.Constants.EMPTY_STRING;

public class DetailsInteractorImpl implements DetailsInteractor {

  private final LocalRepository local;
  private final RemoteRepository remote;

  @Inject DetailsInteractorImpl(Repositories repositories) {
    local = repositories.getLocal();
    remote = repositories.getRemote();
  }

  @Override public Maybe<List<Verse>> getVerses(int chapterNumber) {
    return local.getToken().flatMap(token -> {
      if (token.equals(EMPTY_STRING)) {
        return remote.login().flatMap(newToken -> local.setToken(newToken).andThen(Maybe.just(newToken)));
      }
      return Maybe.just(token);
    }).flatMap(token -> remote.getVerses(token, chapterNumber));
  }

  private Completable logout() {
    return local.setToken(EMPTY_STRING);
  }
}
