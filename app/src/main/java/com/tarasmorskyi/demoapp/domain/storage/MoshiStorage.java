package com.tarasmorskyi.demoapp.domain.storage;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import com.squareup.moshi.Moshi;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

@SuppressWarnings({ "unchecked", "WeakerAccess", "unused" }) public abstract class MoshiStorage {

  private final SharedPreferences.Editor editor;
  private final SharedPreferences preferences;
  private final Moshi moshi;

  @SuppressLint("CommitPrefEdits") MoshiStorage(SharedPreferences preferences, Moshi moshi) {
    this.preferences = preferences;
    this.editor = preferences.edit();
    this.moshi = moshi;
  }

  public <T> void set(String type, T payload, Class<T> clazz) {
    editor.putString(type, moshi.adapter(clazz).toJson(payload));
    editor.commit();
  }

  public <T> void set(T payload, Class<T> clazz) {
    editor.putString(clazz.getSimpleName(), moshi.adapter(clazz).toJson(payload));
    editor.commit();
  }

  public <T> void set(String type, List<T> payload, Class<T[]> clazz) {
    editor.putString(type, moshi.adapter(clazz).toJson((T[]) payload.toArray()));
    editor.commit();
  }

  public <T> void set(String type, HashSet<T> payload, Class<T[]> clazz) {
    editor.putString(type, moshi.adapter(clazz).toJson((T[]) payload.toArray()));
    editor.commit();
  }

  public <T> T get(String type, Class<T> clazz) throws IOException {
    String json = preferences.getString(type, null);
    if (json == null) {
      return null;
    }
    return clazz.cast(moshi.adapter(clazz).fromJson(json));
  }

  public <T> T get(Class<T> clazz) throws IOException {
    String json = preferences.getString(clazz.getSimpleName(), null);
    if (json == null) {
      return null;
    }
    return clazz.cast(moshi.adapter(clazz).fromJson(json));
  }

  public <T> List<T> getList(String type, Class<T[]> clazz) throws IOException {
    String json = preferences.getString(type, null);
    if (json == null) {
      return Collections.emptyList();
    }
    T[] objects = moshi.adapter(clazz).fromJson(json);
    return Arrays.asList(objects);
  }

  public <T> T[] getPrimitiveList(String type, Class<T[]> clazz) throws IOException {
    String json = preferences.getString(type, null);
    if (json == null) {
      return (T[]) Collections.emptyList().toArray();
    }
    return moshi.adapter(clazz).fromJson(json);
  }

  public <T> List<T> getList(String type, Class<T[]> clazz, List<T> defaultValue)
      throws IOException {
    String json = preferences.getString(type, null);
    if (json == null) {
      return defaultValue;
    }
    T[] objects = moshi.adapter(clazz).fromJson(json);
    return Arrays.asList(objects);
  }

  public <T> HashSet<T> getHashSet(String type, Class<T[]> clazz, HashSet<T> defaultValue)
      throws IOException {
    String json = preferences.getString(type, null);
    if (json == null) {
      return defaultValue;
    }
    T[] objects = moshi.adapter(clazz).fromJson(json);

    return new HashSet<>(Arrays.asList(objects));
  }

  public <T> void updateSet(String type, T value, Class<T[]> clazz, boolean add)
      throws IOException {
    HashSet<T> hashSet = getHashSet(type, clazz, new HashSet<T>());
    if (add) {
      if (!hashSet.add(value)) { // equals can not be able to force the replace
        hashSet.remove(value);
        hashSet.add(value);
      }
    } else {
      hashSet.remove(value);
    }
    set(type, hashSet, clazz);
  }

  public <T> T get(String type, Class<T> clazz, T defaultValue) throws IOException {
    String json = preferences.getString(type, null);
    if (json == null) {
      return defaultValue;
    }
    return clazz.cast(moshi.adapter(clazz).fromJson(json));
  }

  public void clearAll() {
    editor.clear();
    editor.commit();
  }

  public void clear(String... values) {
    if (values == null || values.length == 0) {
      return;
    }
    for (String value : values) {
      editor.remove(value);
    }
    editor.commit();
  }

  public void setBoolean(String type, Boolean payload) {
    editor.putBoolean(type, payload);
    editor.commit();
  }

  public void setLong(String type, long payload) {
    editor.putLong(type, payload);
    editor.commit();
  }

  public void setInt(String type, int payload) {
    editor.putInt(type, payload);
    editor.commit();
  }

  public void setFloat(String type, float payload) {
    editor.putFloat(type, payload);
    editor.commit();
  }

  public void setString(String type, String payload) {
    editor.putString(type, payload);
    editor.commit();
  }

  public boolean getBoolean(String type) {
    return preferences.getBoolean(type, false);
  }

  public long getLong(String type) {
    return preferences.getLong(type, 0);
  }

  public int getInt(String type) {
    return preferences.getInt(type, 0);
  }

  public float getFloat(String type) {
    return preferences.getFloat(type, 0.0f);
  }

  public String getString(String type) {
    return preferences.getString(type, null);
  }

  public String getString(String type, String defaultValue) {
    String result = getString(type);
    if (result == null) {
      return defaultValue;
    }
    return result;
  }
}
