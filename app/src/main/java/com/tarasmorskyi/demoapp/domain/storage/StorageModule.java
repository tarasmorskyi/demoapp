package com.tarasmorskyi.demoapp.domain.storage;

import com.squareup.moshi.Moshi;
import com.tarasmorskyi.demoapp.application.App;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

@SuppressWarnings({ "StaticMethodOnlyUsedInOneClass", "squid:S1118", "squid:S1610" }) @Module
public abstract class StorageModule {

  @Provides @Singleton static Storage provideStorage(App application, Moshi moshi) {
    return Storage.getDefault(application, moshi);
  }
}