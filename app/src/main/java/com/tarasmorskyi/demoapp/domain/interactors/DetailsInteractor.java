package com.tarasmorskyi.demoapp.domain.interactors;

import com.tarasmorskyi.demoapp.model.Verse;
import io.reactivex.Maybe;
import java.util.List;

public interface DetailsInteractor {
  Maybe<List<Verse>> getVerses(int chapterNumber);
}
