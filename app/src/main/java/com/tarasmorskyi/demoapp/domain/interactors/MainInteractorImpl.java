package com.tarasmorskyi.demoapp.domain.interactors;

import com.tarasmorskyi.demoapp.domain.repositories.LocalRepository;
import com.tarasmorskyi.demoapp.domain.repositories.RemoteRepository;
import com.tarasmorskyi.demoapp.domain.repositories.Repositories;
import com.tarasmorskyi.demoapp.model.Page;
import io.reactivex.Completable;
import io.reactivex.Maybe;
import java.util.List;
import javax.inject.Inject;

import static com.tarasmorskyi.demoapp.utils.Constants.EMPTY_STRING;

public class MainInteractorImpl implements MainInteractor {

  private final LocalRepository local;
  private final RemoteRepository remote;

  @Inject public MainInteractorImpl(Repositories repositories) {
    local = repositories.getLocal();
    remote = repositories.getRemote();
  }

  @Override public Maybe<List<Page>> getPosts() {
    return local.getToken().flatMap(token -> {
      if (token.equals(EMPTY_STRING)) {
        return remote.login().flatMap(newToken -> local.setToken(newToken).andThen(Maybe.just(newToken)));
      }
      return Maybe.just(token);
    }).flatMap(remote::getPages);
  }

  @Override public Completable logout() {
    return local.setToken(EMPTY_STRING);
  }
}
