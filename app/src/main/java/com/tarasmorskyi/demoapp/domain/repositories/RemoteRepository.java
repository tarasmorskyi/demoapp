package com.tarasmorskyi.demoapp.domain.repositories;

import com.tarasmorskyi.demoapp.model.LoginResult;
import com.tarasmorskyi.demoapp.model.Page;
import com.tarasmorskyi.demoapp.model.Verse;
import io.reactivex.Maybe;
import java.util.List;
import retrofit2.adapter.rxjava2.Result;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RemoteRepository {

  Maybe<String> login();

  Maybe<List<Page>> getPages(String accessToken);

  Maybe<List<Verse>> getVerses(String accessToken, int chapterNumber);

  interface RemoteService {

    @POST("auth/oauth/token") Maybe<Result<LoginResult>> login(
        @Query("client_id") String clientId, @Query("client_secret") String clientSecret,
        @Query("grant_type") String grantType);

    @GET("api/v1/chapters") Maybe<Result<List<Page>>> getPages(
        @Header("Authorization") String accessToken);

    @GET("api/v1/chapters/{num}/verses") Maybe<Result<List<Verse>>> getVerses(
        @Header("Authorization") String accessToken, @Path("num") int chapterNumber);
  }
}
