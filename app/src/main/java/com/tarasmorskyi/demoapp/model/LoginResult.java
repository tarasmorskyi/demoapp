package com.tarasmorskyi.demoapp.model;

import com.google.auto.value.AutoValue;
import com.squareup.moshi.Json;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.tarasmorskyi.demoapp.utils.Constants;

@AutoValue public abstract class LoginResult {
  public static final LoginResult EMPTY = builder()
      .accessToken(Constants.EMPTY_STRING)
      .build();

  public static JsonAdapter<LoginResult> jsonAdapter(Moshi moshi) {
    return new AutoValue_LoginResult.MoshiJsonAdapter(moshi);
  }

  public static Builder builder() {
    return new AutoValue_LoginResult.Builder();
  }

  @Json(name = "access_token") public abstract String accessToken();

  @AutoValue.Builder public abstract static class Builder {
    public abstract Builder accessToken(String accessToken);

    public abstract LoginResult build();
  }
}