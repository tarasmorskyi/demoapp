package com.tarasmorskyi.demoapp.model;

import android.os.Parcelable;
import com.google.auto.value.AutoValue;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.tarasmorskyi.demoapp.utils.Constants;

@AutoValue public abstract class Verse implements Parcelable {
  public static final Verse EMPTY = builder()
      .meaning(Constants.EMPTY_STRING)
      .build();

  public static JsonAdapter<Verse> jsonAdapter(Moshi moshi) {
    return new AutoValue_Verse.MoshiJsonAdapter(moshi);
  }

  public static Builder builder() {
    return new AutoValue_Verse.Builder();
  }

  public abstract String meaning();

  @AutoValue.Builder public abstract static class Builder {

    public abstract Builder meaning(String meaning);

    public abstract Verse build();
  }
}