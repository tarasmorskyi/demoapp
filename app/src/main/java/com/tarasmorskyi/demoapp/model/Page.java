package com.tarasmorskyi.demoapp.model;

import android.os.Parcelable;
import com.google.auto.value.AutoValue;
import com.squareup.moshi.Json;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.tarasmorskyi.demoapp.utils.Constants;

import static com.tarasmorskyi.demoapp.utils.Constants.INVALID;

@AutoValue public abstract class Page implements Parcelable {
  public static final Page EMPTY = builder()
      .number(INVALID)
      .nameMeaning(Constants.EMPTY_STRING)
      .chapterSummary(Constants.EMPTY_STRING)
      .build();

  public static JsonAdapter<Page> jsonAdapter(Moshi moshi) {
    return new AutoValue_Page.MoshiJsonAdapter(moshi);
  }

  public static Builder builder() {
    return new AutoValue_Page.Builder();
  }

  @Json(name = "chapter_number") public abstract int number();

  @Json(name = "name_meaning") public abstract String nameMeaning();

  @Json(name = "chapter_summary") public abstract String chapterSummary();

  @AutoValue.Builder public abstract static class Builder {
    public abstract Builder number(int number);

    public abstract Builder nameMeaning(String nameMeaning);

    public abstract Builder chapterSummary(String chapterSummary);

    public abstract Page build();
  }
}