package com.tarasmorskyi.demoapp.utils;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import com.jakewharton.rxrelay2.BehaviorRelay;
import io.reactivex.Observable;

public class LifecycleHandler implements Application.ActivityLifecycleCallbacks {
  private int started;
  private int stopped;
  private BehaviorRelay<Events> events = BehaviorRelay.create();

  public LifecycleHandler() {
    started = 0;
    stopped = 0;
  }

  @Override
  public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    events.accept(Events.CREATED);
  }

  @Override
  public void onActivityDestroyed(Activity activity) {
    events.accept(Events.CREATED);
  }

  @Override
  public void onActivityResumed(Activity activity) {
    events.accept(Events.RESUMED);
  }

  @Override
  public void onActivityPaused(Activity activity) {
    events.accept(Events.PAUSED);
  }

  @Override
  public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    //not used
  }

  @Override
  public void onActivityStarted(Activity activity) {
    ++started;
    events.accept(Events.STARTED);
  }

  @Override
  public void onActivityStopped(Activity activity) {
    ++stopped;
    events.accept(Events.STOPPED);
  }

  public Observable<Events> getEvents() {
    return events;
  }

  public boolean isApplicationVisible() {
    return started > stopped;
  }

}