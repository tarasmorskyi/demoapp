package com.tarasmorskyi.demoapp.utils;

public enum Events {
  CREATED, STARTED, RESUMED, PAUSED, STOPPED, DESTROYED
}
