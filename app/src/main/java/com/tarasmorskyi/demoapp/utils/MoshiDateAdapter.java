package com.tarasmorskyi.demoapp.utils;

import com.squareup.moshi.FromJson;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.JsonReader;
import com.squareup.moshi.JsonWriter;
import com.squareup.moshi.ToJson;
import java.io.IOException;
import java.util.regex.Pattern;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.temporal.TemporalAccessor;

public final class MoshiDateAdapter extends JsonAdapter<LocalDateTime> {

  private static final Pattern RE_FULL =
      Pattern.compile("\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}");
  private static final Pattern RE_FULL_NO_T =
      Pattern.compile("\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}");
  private static final Pattern RE_FULL_NO_SEC =
      Pattern.compile("\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}");
  private static final Pattern RE_FULL_NO_T_NO_SEC =
      Pattern.compile("\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}");
  private static final Pattern RE_DATE = Pattern.compile("\\d{4}-\\d{2}-\\d{2}");

  private static final DateTimeFormatter YYYY_MM_DDTHH_MM =
      DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
  private static final DateTimeFormatter YYYY_MM_DD_HH_MM_SS =
      DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
  private static final DateTimeFormatter YYYY_MM_DD_HH_MM =
      DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

  @FromJson @Override public synchronized LocalDateTime fromJson(JsonReader reader)
      throws IOException {
    String string = reader.nextString();
    if (string != null && string.length() > 3) {
      TemporalAccessor temporal = getPattern(string).parse(string);
      return LocalDateTime.from(temporal);
    }
    return null;
  }

  private DateTimeFormatter getPattern(CharSequence string) {
    if (RE_FULL.matcher(string).matches()) {
      return DateTimeFormatter.ISO_LOCAL_DATE_TIME;
    } else if (RE_FULL_NO_SEC.matcher(string).matches()) {
      return YYYY_MM_DDTHH_MM;
    } else if (RE_FULL_NO_T.matcher(string).matches()) {
      return YYYY_MM_DD_HH_MM_SS;
    } else if (RE_FULL_NO_T_NO_SEC.matcher(string).matches()) {
      return YYYY_MM_DD_HH_MM;
    } else if (RE_DATE.matcher(string).matches()) {
      return DateTimeFormatter.ISO_LOCAL_DATE;
    } else {
      return DateTimeFormatter.ISO_TIME;
    }
  }

  @ToJson @Override public synchronized void toJson(JsonWriter writer, LocalDateTime value)
      throws IOException {
    String string = value.toString();
    writer.value(string);
  }
}