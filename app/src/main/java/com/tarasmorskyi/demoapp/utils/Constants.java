package com.tarasmorskyi.demoapp.utils;

import com.tarasmorskyi.demoapp.utils.errors.MethodNotImplemented;

public class Constants {
  public static final String EMPTY_STRING = "";
  public static final int INVALID = -1;
  public static final MethodNotImplemented METHOD_NOT_IMPLEMENTED = new MethodNotImplemented();
}
