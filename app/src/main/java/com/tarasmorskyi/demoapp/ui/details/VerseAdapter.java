package com.tarasmorskyi.demoapp.ui.details;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.tarasmorskyi.demoapp.R;
import com.tarasmorskyi.demoapp.databinding.VerseItemBinding;
import com.tarasmorskyi.demoapp.di.ActivityScope;
import com.tarasmorskyi.demoapp.model.Verse;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.annotations.NonNull;
import io.reactivex.subjects.PublishSubject;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;

@ActivityScope public class VerseAdapter
    extends RecyclerView.Adapter<VerseAdapter.ContactViewHolder> {

  private final PublishSubject<Verse> onClick = PublishSubject.create();
  private LayoutInflater inflater;
  private List<Verse> verses;

  @Inject public VerseAdapter(LayoutInflater inflater) {
    this.inflater = inflater;
    verses = Collections.emptyList();
  }

  @Override public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    VerseItemBinding binding =
        DataBindingUtil.inflate(inflater, R.layout.verse_item, parent, false);
    return new ContactViewHolder(binding);
  }

  @Override public void onBindViewHolder(ContactViewHolder holder, int position) {
    holder.setVerse(verses.get(position));
  }

  @Override public void onViewDetachedFromWindow(ContactViewHolder holder) {
    super.onViewDetachedFromWindow(holder);
    holder.unbind();
  }

  @Override public int getItemCount() {
    return verses.size();
  }

  public void setItems(@NonNull List<Verse> verses) {
    this.verses = verses;
    notifyDataSetChanged();
  }

  public Flowable<Verse> getClicks() {
    return onClick.toFlowable(BackpressureStrategy.LATEST);
  }

  class ContactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private final VerseItemBinding binding;
    private Verse verse;

    ContactViewHolder(VerseItemBinding binding) {
      super(binding.getRoot());
      this.binding = binding;
    }

    void setVerse(Verse verse) {
      this.verse = verse;
      binding.title.setText(verse.meaning());
      binding.root.setOnClickListener(this);
    }

    void unbind() {
      binding.getRoot().setOnClickListener(null);
    }

    @Override public void onClick(View view) {
      switch (view.getId()){
        case R.id.root:
          onClick.onNext(verse);
          break;
      }
    }
  }
}