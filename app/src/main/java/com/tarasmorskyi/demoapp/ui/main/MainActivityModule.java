package com.tarasmorskyi.demoapp.ui.main;

import dagger.Module;

@SuppressWarnings({ "StaticMethodOnlyUsedInOneClass", "squid:S1118", "squid:S1610" }) @Module
public abstract class MainActivityModule {
}
