package com.tarasmorskyi.demoapp.ui.main;

import com.tarasmorskyi.demoapp.ui.base.BaseView;
import io.reactivex.Observer;

interface MainView extends BaseView<MainUiModel>, Observer<MainUiModel> {

}
