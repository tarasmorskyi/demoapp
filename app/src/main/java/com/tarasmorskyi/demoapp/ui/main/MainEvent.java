package com.tarasmorskyi.demoapp.ui.main;

import android.support.annotation.IntDef;
import com.google.auto.value.AutoValue;
import com.tarasmorskyi.demoapp.model.Page;
import com.tarasmorskyi.demoapp.ui.base.BaseEvent;
import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

@AutoValue abstract class MainEvent implements BaseEvent {

  static final int LOADED = 1;
  static final int LIST_CLICKED = 2;

  static final MainEvent VIEW_LOADED_EVENT = emptyEvent().event(LOADED).build();

  static MainEvent onListClicked(Page page){
    return emptyEvent().event(LIST_CLICKED).page(page).build();
  }

  private static Builder emptyEvent() {
    return new AutoValue_MainEvent.Builder().event(NO_EVENT).page(Page.EMPTY);
  }

  @Events public abstract int event();

  public abstract Page page();

  @Retention(SOURCE) @IntDef({ NO_EVENT, LOADED, LIST_CLICKED }) @interface Events {
  }

  @AutoValue.Builder abstract static class Builder {
    public abstract Builder event(@Events int event);

    public abstract Builder page(Page page);

    public abstract MainEvent build();
  }
}
