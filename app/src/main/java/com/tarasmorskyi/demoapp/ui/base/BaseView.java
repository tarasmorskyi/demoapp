package com.tarasmorskyi.demoapp.ui.base;

public interface BaseView<M extends BaseUiModel> {
  void render(M viewModel);
}
