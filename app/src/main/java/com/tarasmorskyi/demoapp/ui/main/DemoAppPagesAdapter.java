package com.tarasmorskyi.demoapp.ui.main;

import android.databinding.DataBindingUtil;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.tarasmorskyi.demoapp.R;
import com.tarasmorskyi.demoapp.di.ActivityScope;
import com.tarasmorskyi.demoapp.model.Page;
import com.tarasmorskyi.demoapp.databinding.PageItemBinding;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.subjects.PublishSubject;
import java.util.Collections;
import java.util.List;
import javax.inject.Inject;

@ActivityScope public class DemoAppPagesAdapter
    extends RecyclerView.Adapter<DemoAppPagesAdapter.ContactViewHolder> {

  private final PublishSubject<Page> onClick = PublishSubject.create();
  private LayoutInflater inflater;
  private List<Page> pages;

  @Inject public DemoAppPagesAdapter(LayoutInflater inflater) {
    this.inflater = inflater;
    pages = Collections.emptyList();
  }

  @Override public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    PageItemBinding binding =
        DataBindingUtil.inflate(inflater, R.layout.page_item, parent, false);
    return new ContactViewHolder(binding);
  }

  @Override public void onBindViewHolder(ContactViewHolder holder, int position) {
    holder.setPage(pages.get(position));
  }

  @Override public void onViewDetachedFromWindow(ContactViewHolder holder) {
    super.onViewDetachedFromWindow(holder);
    holder.unbind();
  }

  @Override public int getItemCount() {
    return pages.size();
  }

  public void setItems(@NonNull List<Page> pages) {
    this.pages = pages;
    notifyDataSetChanged();
  }

  public Flowable<Page> getClicks() {
    return onClick.toFlowable(BackpressureStrategy.LATEST);
  }

  class ContactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private final PageItemBinding binding;
    private Page page;

    ContactViewHolder(PageItemBinding binding) {
      super(binding.getRoot());
      this.binding = binding;
    }

    void setPage(Page page) {
      this.page = page;
      binding.title.setText(page.nameMeaning());
      binding.content.setText(page.chapterSummary());
      binding.root.setOnClickListener(this);
    }

    void unbind() {
      binding.getRoot().setOnClickListener(null);
    }

    @Override public void onClick(View view) {
      switch (view.getId()){
        case R.id.root:
          onClick.onNext(page);
          break;
      }
    }
  }
}