package com.tarasmorskyi.demoapp.ui.main;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import com.tarasmorskyi.demoapp.R;
import com.tarasmorskyi.demoapp.databinding.ActivityMainBinding;
import com.tarasmorskyi.demoapp.di.ActivityScope;
import com.tarasmorskyi.demoapp.model.Page;
import com.tarasmorskyi.demoapp.ui.base.BaseActivity;
import com.tarasmorskyi.demoapp.ui.base.BaseUiModel;
import com.tarasmorskyi.demoapp.ui.details.DetailsActivity;
import io.reactivex.disposables.Disposable;
import java.util.List;
import javax.inject.Inject;
import timber.log.Timber;

@SuppressWarnings("squid:MaximumInheritanceDepth") @ActivityScope public class MainActivity
    extends BaseActivity<MainUiModel, MainEvent> implements MainView {

  @Inject MainPresenter presenter;
  private ActivityMainBinding binding;
  @Inject DemoAppPagesAdapter adapter;
  private Disposable clicksStream;

  public static Intent createIntent(Context context) {
    Intent intent = new Intent(context, MainActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    return intent;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    //analytics init
    binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
    presenter.attach(this).compose(this::setDefaults).subscribe(this);

    binding.list.setAdapter(adapter);
    clicksStream = adapter.getClicks()
        .map(MainEvent::onListClicked)
        .toObservable()
        .subscribe(this::sendEvent);
  }

  @Override protected void onResume() {
    super.onResume();
    sendEvent(MainEvent.VIEW_LOADED_EVENT);
  }

  @Override protected void sendEvent(MainEvent event) {
    presenter.event(event);
  }

  @Override public void onNext(MainUiModel uiModel) {
    render(uiModel);
  }

  @Override public void render(MainUiModel uiModel) {
    hideProgress();
    switch (uiModel.model()) {
      case MainUiModel.FAILURE:
        showWarningMessage(uiModel.message());
        break;
      case MainUiModel.LOADED:
        setItems(uiModel.pages());
        break;
      case MainUiModel.LOADING:
        showProgress(getString(R.string.loading));
        break;
      case MainUiModel.OPEN_DETAILS:
        startActivity(DetailsActivity.createIntent(this, uiModel.page()));
        break;
      case BaseUiModel.INVALID:
      default:
        Timber.w("render: unhandled [uiModel %s]", uiModel);
        break;
    }
    super.render(uiModel);
  }

  private void setItems(List<Page> pages) {
    adapter.setItems(pages);
  }

  private void showWarningMessage(CharSequence message) {
    Timber.d("showWarningMessage() called  with: messageText = [%s]", message);
    Snackbar.make(binding.getRoot(), message, Snackbar.LENGTH_LONG).show();
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    if (!clicksStream.isDisposed())
      clicksStream.dispose();
  }
}