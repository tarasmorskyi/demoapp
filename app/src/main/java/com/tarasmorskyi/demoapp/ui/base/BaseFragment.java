package com.tarasmorskyi.demoapp.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.design.widget.BottomSheetBehavior;
import com.trello.rxlifecycle2.android.FragmentEvent;
import com.trello.rxlifecycle2.components.support.RxFragment;
import dagger.android.support.AndroidSupportInjection;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import timber.log.Timber;

public abstract class BaseFragment<M extends BaseUiModel, E extends BaseEvent> extends RxFragment
    implements Observer<M>, BaseView<M> {

  //same reason as in BaseActivity
  /*protected final LifecycleProvider<Lifecycle.Event> provider =
      AndroidLifecycle.createLifecycleProvider(this);*/

  @BottomSheetBehavior.State M lastUiModel;
  private CompositeDisposable disposables = new CompositeDisposable();

  @CallSuper @Override public void onAttach(Context context) {
    AndroidSupportInjection.inject(this);
    super.onAttach(context);
  }

  @CallSuper @Override public void onDetach() {
    super.onDetach();
    if (!disposables.isDisposed()) {
      disposables.dispose();
    }
  }

  @Override public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
  }

  protected abstract void sendEvent(E event);

  @Override public void onSubscribe(Disposable d) {
    disposables.add(d);
  }

  @Override public void onError(Throwable throwable) {
    Timber.d(throwable, "onError() called");
  }

  @Override public void onComplete() { /*ignored*/ }

  @CallSuper @Override public void render(M uiModel) {
    if (lastUiModel == null || !lastUiModel.equals(uiModel)) {
      lastUiModel = uiModel;
    }
  }

  protected boolean isLastUiModel() {
    return lastUiModel != null;
  }

  protected M getLastUiModel() {
    return lastUiModel;
  }

  public <O> ObservableSource<O> setDefaults(Observable<O> observable) {
    /*return observable.compose(provider.bindUntilEvent(Lifecycle.Event.ON_STOP))
        .observeOn(AndroidSchedulers.mainThread());*/
    return observable.compose(bindUntilEvent(FragmentEvent.STOP))
        .observeOn(AndroidSchedulers.mainThread());
  }
}
