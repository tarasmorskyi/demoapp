package com.tarasmorskyi.demoapp.ui.details;

import com.tarasmorskyi.demoapp.domain.interactors.DetailsInteractor;
import com.tarasmorskyi.demoapp.ui.base.BaseEvent;
import com.tarasmorskyi.demoapp.ui.base.BasePresenter;
import com.tarasmorskyi.demoapp.utils.Constants;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import javax.inject.Inject;
import timber.log.Timber;

class DetailsPresenter extends BasePresenter<DetailsView, DetailsEvent, DetailsUiModel> {

  private final DetailsInteractor interactor;
  //Lazy injection required because RxPermissions have to be initialized after super.onCreate()

  @Inject DetailsPresenter(DetailsInteractor interactor) {
    this.interactor = interactor;
  }

  @SuppressWarnings({ "squid:S1185" }) @Override
  public Observable<DetailsUiModel> attach(DetailsView view) {
    return super.attach(view);
    //method must be visible to package
  }

  @SuppressWarnings({ "squid:S1185" }) @Override protected void detach() {
    super.detach();
    //method must be visible to package
  }

  @Override protected Observable<DetailsUiModel> getModel() {
    return events.flatMap(this::onEvent);
  }

  private ObservableSource<DetailsUiModel> onEvent(DetailsEvent event) {
    Timber.d("event() called  with: event = [%s]", event);
    switch (event.event()) {
      case DetailsEvent.LOADED:
        return loadVerses(event.page().number());
      case BaseEvent.NO_EVENT:
      default:
        Timber.e("event %s unhandled", event);
        return Observable.error(Constants.METHOD_NOT_IMPLEMENTED);
    }
  }

  private Observable<DetailsUiModel> loadVerses(int chapterNumber) {
    return interactor.getVerses(chapterNumber)
        .map(DetailsUiModel::onLoaded)
        .toObservable()
        .startWith(DetailsUiModel.ON_LOADING);
  }
}
