package com.tarasmorskyi.demoapp.ui.base;

import android.support.annotation.CallSuper;
import com.jakewharton.rxrelay2.PublishRelay;
import io.reactivex.Observable;

public abstract class BasePresenter<V extends BaseView, E extends BaseEvent, M extends BaseUiModel>
    implements Presenter<E, M> {

  protected final PublishRelay<E> events = PublishRelay.create();
  protected V view;

  protected BasePresenter() {
  }

  @CallSuper protected Observable<M> attach(V view) {
    this.view = view;
    return getModel();
  }

  @CallSuper protected void detach() {
    view = null;
  }

  @CallSuper public void event(E event) {
    events.accept(event);
  }

  protected abstract Observable<M> getModel();
}
