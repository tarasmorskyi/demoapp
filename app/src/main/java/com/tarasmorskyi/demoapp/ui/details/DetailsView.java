package com.tarasmorskyi.demoapp.ui.details;

import com.tarasmorskyi.demoapp.ui.base.BaseView;
import io.reactivex.Observer;

interface DetailsView extends BaseView<DetailsUiModel>, Observer<DetailsUiModel> {

}
