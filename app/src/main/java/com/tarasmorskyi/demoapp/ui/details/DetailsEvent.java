package com.tarasmorskyi.demoapp.ui.details;

import android.support.annotation.IntDef;
import com.google.auto.value.AutoValue;
import com.tarasmorskyi.demoapp.model.Page;
import com.tarasmorskyi.demoapp.ui.base.BaseEvent;
import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

@AutoValue abstract class DetailsEvent implements BaseEvent {

  static final int LOADED = 1;

  static DetailsEvent onLoaded(Page page){
    return emptyEvent().event(LOADED).page(page).build();
  }

  private static Builder emptyEvent() {
    return new AutoValue_DetailsEvent.Builder().event(NO_EVENT).page(Page.EMPTY);
  }

  @Events public abstract int event();

  public abstract Page page();

  @Retention(SOURCE) @IntDef({ NO_EVENT, LOADED }) @interface Events {
  }

  @AutoValue.Builder abstract static class Builder {
    public abstract Builder event(@Events int event);

    public abstract Builder page(Page page);

    public abstract DetailsEvent build();
  }
}
