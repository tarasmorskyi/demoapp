package com.tarasmorskyi.demoapp.ui.details;

import android.support.annotation.IntDef;
import com.google.auto.value.AutoValue;
import com.tarasmorskyi.demoapp.model.Verse;
import com.tarasmorskyi.demoapp.ui.base.BaseUiModel;
import java.lang.annotation.Retention;
import java.util.Collections;
import java.util.List;

import static com.tarasmorskyi.demoapp.utils.Constants.EMPTY_STRING;
import static java.lang.annotation.RetentionPolicy.SOURCE;

@AutoValue abstract class DetailsUiModel implements BaseUiModel {

  static final int FAILURE = 1;
  static final int LOADED = 2;
  static final int LOADING = 3;

  static final DetailsUiModel ON_LOADING = emptyBuilder().model(LOADING).build();

  static DetailsUiModel onFailure(String message) {
    return emptyBuilder().message(message).model(FAILURE).build();
  }

  static DetailsUiModel onLoaded(List<Verse> verses) {
    return emptyBuilder().verses(verses).model(LOADED).build();
  }

  private static Builder emptyBuilder() {
    return new $AutoValue_DetailsUiModel.Builder().model(INVALID)
        .message(EMPTY_STRING)
        .verses(Collections.emptyList());
  }

  @Models public abstract int model();

  abstract String message();

  abstract List<Verse> verses();

  @Retention(SOURCE) @IntDef({ INVALID, FAILURE, LOADED, LOADING }) @interface Models {
  }

  @AutoValue.Builder public abstract static class Builder {

    public abstract Builder model(@Models int model);

    public abstract Builder message(String message);

    public abstract Builder verses(List<Verse> verses);

    public abstract DetailsUiModel build();
  }
}
