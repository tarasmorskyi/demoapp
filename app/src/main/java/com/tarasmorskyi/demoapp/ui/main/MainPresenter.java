package com.tarasmorskyi.demoapp.ui.main;

import com.tarasmorskyi.demoapp.domain.interactors.MainInteractor;
import com.tarasmorskyi.demoapp.ui.base.BaseEvent;
import com.tarasmorskyi.demoapp.ui.base.BasePresenter;
import com.tarasmorskyi.demoapp.utils.Constants;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import javax.inject.Inject;
import timber.log.Timber;

class MainPresenter extends BasePresenter<MainView, MainEvent, MainUiModel> {

  private final MainInteractor interactor;
  //Lazy injection required because RxPermissions have to be initialized after super.onCreate()

  @Inject MainPresenter(MainInteractor interactor) {
    this.interactor = interactor;
  }

  @SuppressWarnings({ "squid:S1185" }) @Override
  public Observable<MainUiModel> attach(MainView view) {
    return super.attach(view);
    //method must be visible to package
  }

  @SuppressWarnings({ "squid:S1185" }) @Override protected void detach() {
    super.detach();
    //method must be visible to package
  }

  @Override protected Observable<MainUiModel> getModel() {
    return events.flatMap(this::onEvent);
  }

  private ObservableSource<MainUiModel> onEvent(MainEvent event) {
    Timber.d("event() called  with: event = [%s]", event);
    switch (event.event()) {
      case MainEvent.LOADED:
        return loadPosts();
      case MainEvent.LIST_CLICKED:
        return Observable.just(MainUiModel.onOpenDetails(event.page()));
      case BaseEvent.NO_EVENT:
      default:
        Timber.e("event %s unhandled", event);
        return Observable.error(Constants.METHOD_NOT_IMPLEMENTED);
    }
  }

  private Observable<MainUiModel> loadPosts() {
    return interactor.getPosts()
        .map(MainUiModel::onLoaded)
        .toObservable()
        .onErrorResumeNext(interactor.logout()
            .andThen(interactor.getPosts().map(MainUiModel::onLoaded))
            .toObservable())
        .startWith(MainUiModel.ON_LOADING);
  }
}
