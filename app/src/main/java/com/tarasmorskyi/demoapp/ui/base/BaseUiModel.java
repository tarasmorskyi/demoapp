package com.tarasmorskyi.demoapp.ui.base;

import android.os.Parcelable;

public interface BaseUiModel extends Parcelable {
  int INVALID = -1;

  int model();
}
