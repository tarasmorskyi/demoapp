package com.tarasmorskyi.demoapp.ui.details;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import com.tarasmorskyi.demoapp.R;
import com.tarasmorskyi.demoapp.databinding.ActivityDetailsBinding;
import com.tarasmorskyi.demoapp.di.ActivityScope;
import com.tarasmorskyi.demoapp.model.Page;
import com.tarasmorskyi.demoapp.model.Verse;
import com.tarasmorskyi.demoapp.ui.base.BaseActivity;
import com.tarasmorskyi.demoapp.ui.base.BaseUiModel;
import io.reactivex.disposables.Disposable;
import java.util.List;
import javax.inject.Inject;
import timber.log.Timber;

@SuppressWarnings("squid:MaximumInheritanceDepth") @ActivityScope public class DetailsActivity
    extends BaseActivity<DetailsUiModel, DetailsEvent> implements DetailsView {

  private static final String PAGE = "page";
  @Inject DetailsPresenter presenter;
  private ActivityDetailsBinding binding;
  @Inject VerseAdapter adapter;
  private Disposable clicksStream;

  public static Intent createIntent(Context context, Page page) {
    Intent intent = new Intent(context, DetailsActivity.class);
    intent.putExtra(PAGE, page);
    return intent;
  }

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    //analytics init
    binding = DataBindingUtil.setContentView(this, R.layout.activity_details);
    presenter.attach(this).compose(this::setDefaults).subscribe(this);

    binding.list.setAdapter(adapter);
  }

  @Override protected void onResume() {
    super.onResume();
    sendEvent(DetailsEvent.onLoaded(getIntent().getParcelableExtra(PAGE)));
  }

  @Override protected void sendEvent(DetailsEvent event) {
    presenter.event(event);
  }

  @Override public void onNext(DetailsUiModel uiModel) {
    render(uiModel);
  }

  @Override public void render(DetailsUiModel uiModel) {
    hideProgress();
    switch (uiModel.model()) {
      case DetailsUiModel.FAILURE:
        showWarningMessage(uiModel.message());
        break;
      case DetailsUiModel.LOADED:
        setItems(uiModel.verses());
        break;
      case DetailsUiModel.LOADING:
        showProgress(getString(R.string.loading));
        break;
      case BaseUiModel.INVALID:
      default:
        Timber.w("render: unhandled [uiModel %s]", uiModel);
        break;
    }
    super.render(uiModel);
  }

  private void setItems(List<Verse> verses) {
    adapter.setItems(verses);
  }

  private void showWarningMessage(CharSequence message) {
    Timber.d("showWarningMessage() called  with: messageText = [%s]", message);
    Snackbar.make(binding.getRoot(), message, Snackbar.LENGTH_LONG).show();
  }
}