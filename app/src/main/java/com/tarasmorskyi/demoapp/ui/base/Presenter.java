package com.tarasmorskyi.demoapp.ui.base;

interface Presenter<E extends BaseEvent, M extends BaseUiModel> {
  void event(E event);
}
