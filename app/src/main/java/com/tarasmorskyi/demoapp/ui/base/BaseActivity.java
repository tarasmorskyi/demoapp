package com.tarasmorskyi.demoapp.ui.base;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import com.tarasmorskyi.demoapp.application.App;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.DaggerAppCompatActivity;
import dagger.android.support.HasSupportFragmentInjector;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import javax.inject.Inject;
import timber.log.Timber;

@SuppressLint("Registered") @SuppressWarnings("squid:MaximumInheritanceDepth")
public abstract class BaseActivity<M extends BaseUiModel, E extends BaseEvent>
    extends DaggerAppCompatActivity
    implements Observer<M>, BaseView<M>, HasSupportFragmentInjector {

  protected CompositeDisposable disposables = new CompositeDisposable();
  protected ProgressDialog progressDialog;
  @Inject DispatchingAndroidInjector<Fragment> fragmentInjector;
  @Inject App appInstance;
  M lastUiModel;

  @Override protected void attachBaseContext(Context newBase) {
    super.attachBaseContext(newBase);
  }

  @Override protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override protected void onPostCreate(@Nullable Bundle savedInstanceState) {
    super.onPostCreate(savedInstanceState);
  }

  @Override public void onSaveInstanceState(Bundle outState) {
    try {
      super.onSaveInstanceState(outState);
    }catch (IllegalStateException ignore){};
  }

  @Override protected void onStart() {
    super.onStart();
  }

  @Override protected void onResume() {
    super.onResume();
  }


  @Override protected void onPause() {
    super.onPause();
  }

  @Override protected void onStop() {
    Timber.d("onStop: []");
    super.onStop();
  }

  @Override protected void onDestroy() {
    super.onDestroy();
    if (!disposables.isDisposed()) {
      disposables.dispose();
    }
    if (progressDialog != null) {
      progressDialog.dismiss();
    }
  }

  @CallSuper @Override public void onSubscribe(Disposable d) {
    Timber.d("onSubscribe() called  with: disposable = [%s]", d);
    disposables.add(d);
  }

  /**
   * Override to catch errors on events
   */
  @Override public void onError(Throwable throwable) {
    Timber.d("onError: [throwable]");
    hideProgress();
  }

  /**
   * Override to catch onComplete event from events
   */
  @Override public void onComplete() {
    Timber.d("onComplete: []");
    /*ignored*/ }

  /**
   * Call super to save ui state for restoring
   */
  @CallSuper @Override public void render(M uiModel) {
    if (lastUiModel == null || !lastUiModel.equals(uiModel)) {
      lastUiModel = uiModel;
    }
  }

  @Override public AndroidInjector<Fragment> supportFragmentInjector() {
    return fragmentInjector;
  }

  protected boolean isLastUiModel() {
    return lastUiModel != null;
  }

  protected M getLastUiModel() {
    return lastUiModel;
  }

  /**
   * Override to send events to presenter. For the most cases use inside:
   * presenter.event(...).compose(this::setDefaults).subscribe(this);
   * that will ensure m\odels are observed on the Main Thread
   * and subscriptions are held until ON_PAUSE events
   */
  protected abstract void sendEvent(E event);

  public <T> ObservableSource<T> setDefaults(Observable<T> observable) {
    Observable<T> converted = observable;
    return converted.observeOn(AndroidSchedulers.mainThread());
  }

  public void showProgress(String message) {
    if (progressDialog == null) {
      progressDialog = new ProgressDialog(this);
      progressDialog.setCancelable(false);
    }
    progressDialog.setMessage(message);
    progressDialog.show();
  }

  public void hideProgress() {
    if (progressDialog != null) {
      progressDialog.dismiss();
    }
  }

}
