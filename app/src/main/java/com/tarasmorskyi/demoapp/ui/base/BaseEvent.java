package com.tarasmorskyi.demoapp.ui.base;

public interface BaseEvent {
  int NO_EVENT = 0;

  int event();
}
