package com.tarasmorskyi.demoapp.ui.main;

import android.support.annotation.IntDef;
import com.google.auto.value.AutoValue;
import com.tarasmorskyi.demoapp.model.Page;
import com.tarasmorskyi.demoapp.ui.base.BaseUiModel;
import java.lang.annotation.Retention;
import java.util.Collections;
import java.util.List;

import static com.tarasmorskyi.demoapp.utils.Constants.EMPTY_STRING;
import static java.lang.annotation.RetentionPolicy.SOURCE;

@AutoValue abstract class MainUiModel implements BaseUiModel {

  static final int FAILURE = 1;
  static final int LOADED = 2;
  static final int LOADING = 3;
  static final int OPEN_DETAILS = 4;

  static final MainUiModel ON_LOADING = emptyBuilder().model(LOADING).build();

  static MainUiModel onFailure(String message) {
    return emptyBuilder().message(message).model(FAILURE).build();
  }

  static MainUiModel onLoaded(List<Page> pages) {
    return emptyBuilder().pages(pages).model(LOADED).build();
  }

  static MainUiModel onOpenDetails(Page page){
    return emptyBuilder().model(OPEN_DETAILS).page(page).build();
  }

  private static Builder emptyBuilder() {
    return new $AutoValue_MainUiModel.Builder().model(INVALID)
        .message(EMPTY_STRING)
        .pages(Collections.emptyList())
        .page(Page.EMPTY);
  }

  @Models public abstract int model();

  abstract String message();

  abstract List<Page> pages();

  abstract Page page();

  @Retention(SOURCE) @IntDef({ INVALID, FAILURE, LOADED, LOADING, OPEN_DETAILS }) @interface Models {
  }

  @AutoValue.Builder public abstract static class Builder {

    public abstract Builder model(@Models int model);

    public abstract Builder message(String message);

    public abstract Builder pages(List<Page> pages);

    public abstract Builder page(Page page);

    public abstract MainUiModel build();
  }
}
