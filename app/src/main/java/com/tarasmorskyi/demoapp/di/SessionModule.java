package com.tarasmorskyi.demoapp.di;

import com.tarasmorskyi.demoapp.application.App;
import com.tarasmorskyi.demoapp.domain.interactors.DomainInteractorsModule;
import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@SuppressWarnings({ "StaticMethodOnlyUsedInOneClass", "squid:S1118", "squid:S1610" })
@Module(subcomponents = { SessionModule.SessionComponent.class }) abstract class SessionModule {

  @Provides static SessionComponent sessionComponentBuilder(SessionComponent.Builder builder) {
    return builder.build();
  }

  @Binds abstract AndroidInjector<App> injector(SessionComponent component);

  @SessionScope @Subcomponent(modules = {
      DomainInteractorsModule.class, ActivitiesModule.class, AndroidSupportInjectionModule.class
  }) interface SessionComponent extends AndroidInjector<App> {
    @Subcomponent.Builder abstract class Builder {
      abstract SessionComponent build();
    }
  }
}
