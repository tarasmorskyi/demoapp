package com.tarasmorskyi.demoapp.di;

import com.tarasmorskyi.demoapp.ui.details.DetailsActivity;
import com.tarasmorskyi.demoapp.ui.details.DetailsActivityModule;
import com.tarasmorskyi.demoapp.ui.main.MainActivity;
import com.tarasmorskyi.demoapp.ui.main.MainActivityModule;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@SuppressWarnings({ "StaticMethodOnlyUsedInOneClass", "squid:S1118", "squid:S1610" }) @Module
abstract class ActivitiesModule {

  @ActivityScope @ContributesAndroidInjector(modules = { MainActivityModule.class })
  abstract MainActivity mainActivity();

  @ActivityScope @ContributesAndroidInjector(modules = { DetailsActivityModule.class })
  abstract DetailsActivity detailsActivity();

}

