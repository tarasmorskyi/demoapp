package com.tarasmorskyi.demoapp.di;

import dagger.releasablereferences.CanReleaseReferences;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import javax.inject.Scope;

@Scope @Retention(RetentionPolicy.RUNTIME) @CanReleaseReferences public @interface ActivityScope {
}