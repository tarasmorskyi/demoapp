package com.tarasmorskyi.demoapp.di;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import com.tarasmorskyi.demoapp.application.App;
import com.tarasmorskyi.demoapp.utils.LifecycleHandler;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

@Module abstract class ApplicationModule {

  @Provides @Singleton static LayoutInflater provideLayoutInflater(App app) {
    return (LayoutInflater) app.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  }

  @Provides @Singleton static LifecycleHandler provideLifecycleHandler(App app) {
    return new LifecycleHandler();
  }

  @Provides @Singleton static Resources provideResources(App app) {
    return app.getResources();
  }
}