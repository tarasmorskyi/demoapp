package com.tarasmorskyi.demoapp.di;

import javax.inject.Scope;

@Scope public @interface SessionScope {
}
