package com.tarasmorskyi.demoapp.di;

import com.tarasmorskyi.demoapp.application.App;
import com.tarasmorskyi.demoapp.domain.DomainToolsModule;
import com.tarasmorskyi.demoapp.domain.repositories.RepositoryModule;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import javax.inject.Singleton;

@Singleton @Component(modules = {
    DomainToolsModule.class, RepositoryModule.class, AndroidSupportInjectionModule.class,
    SessionModule.class, ApplicationModule.class
}) public interface AppComponent extends AndroidInjector<App> {

  AndroidInjector<App> androidInjector();

  @Component.Builder abstract class Builder extends AndroidInjector.Builder<App> {
    @Override public abstract AppComponent build();
  }
}