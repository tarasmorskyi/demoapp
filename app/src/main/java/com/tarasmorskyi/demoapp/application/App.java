package com.tarasmorskyi.demoapp.application;

import android.content.Context;
import android.support.multidex.MultiDex;
import com.jakewharton.threetenabp.AndroidThreeTen;
import com.tarasmorskyi.demoapp.di.AppComponent;
import com.tarasmorskyi.demoapp.di.DaggerAppComponent;
import com.tarasmorskyi.demoapp.utils.LifecycleHandler;
import dagger.Lazy;
import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import javax.inject.Inject;

public class App extends DaggerApplication {

  private static App applicationClass;

  @Inject Lazy<ApplicationEnvironment> applicationEnvironment;
  @Inject LifecycleHandler lifecycleHandler;
  private AppComponent appComponent;

  public App() {
    applicationClass = this;
  }

  public static App getInstance() {
    return applicationClass;
  }


  @Override
  protected void attachBaseContext(Context base) {
    super.attachBaseContext(base);
    MultiDex.install(this);
  }

  @Override public void onCreate() {
    super.onCreate();
    AndroidThreeTen.init(this);
    applicationEnvironment.get().init();
    registerActivityLifecycleCallbacks(lifecycleHandler);
  }

  @Override protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
    AppComponent.Builder builder = DaggerAppComponent.builder();
    builder.seedInstance(this);
    appComponent = builder.build();
    return appComponent.androidInjector();
  }

  public AppComponent appComponent() {
    return appComponent;
  }

}