package com.tarasmorskyi.demoapp.application;

import android.util.Log;
import com.facebook.stetho.Stetho;
import com.tarasmorskyi.demoapp.BuildConfig;
import io.reactivex.CompletableObserver;
import io.reactivex.disposables.Disposable;
import javax.inject.Inject;
import timber.log.Timber;

public class ApplicationEnvironment implements CompletableObserver {

  private static final int ONE_FRAME = 16;
  private final App app;

  @Inject ApplicationEnvironment(App app) {
    this.app = app;
  }

  void init() {
    if (BuildConfig.DEBUG) {
      Timber.plant(new Timber.DebugTree());
      Stetho.initializeWithDefaults(app);
    } else {
      Timber.plant(new CrashReportingTree());
    }
  }

  @Override public void onSubscribe(Disposable d) {
    //ignored
  }

  @Override public void onComplete() {
    Timber.d("delayed loading finished");
  }

  @Override public void onError(Throwable e) {
    Timber.e(e, "onError() called");
  }

  private static class CrashReportingTree extends Timber.Tree {
    CrashReportingTree() {
    }

    @Override protected void log(int priority, String tag, String message, Throwable t) {
      if (priority == Log.VERBOSE || priority == Log.DEBUG) {
        return;
      }
    }
  }
}
