This project is created for demonstrating my main skills in Android development.
It's main purpose is to show how I use most useful and popular libraries as Retrofit, Dagger2 and RxJava.

Project uses API which I found in public API list on github (https://github.com/toddmotto/public-apis).
The reason why this one was chosen wasn't for sure by topic but for using Oauth which returns token by
simple authorization by providing clientId and clientSecret in POST request instead of using redirection
like other APIs do.
After getting token app loads chapters from API and displays it in RecyclerView. This request is starting after
getting token from SharedPreferences or API and creates something like a chain which is easily supported,
is Null friendly with AutoValue and is easy to manage errors without breaking chain.
This architecture is a great addition for MVVM (ViewModel is called "Presenter" in my app) and Clean Architecture
to create Reactive Architecture which works great with Android's Lifecycle.